window.onload = () => {
  let places = staticLoadPlaces();
  renderPlaces(places);
}

function staticLoadPlaces() {
  return [
    {
      name: 'Kost WARARAARA',
      location: {
        lat: -7.29262,
        lng: 112.74997,
      }
    }
  ]
}

function renderPlaces(places) {
  let scene = document.querySelector('a-scene');
  places.forEach((place) => {
    let latitude = place.location.lat;
    let longitude = place.location.lng;
    
    let model = document.createElement('a-video');
    model.setAttribute('gps-entity-place', `latitude: ${latitude}; longitude: ${longitude};`)
    model.setAttribute('id', 'video')
    model.setAttribute('src', '#video-src')
    model.setAttribute('position', '0 180 0')
    model.setAttribute('rotation', '0 20 0')
    model.setAttribute('width', '8')
    model.setAttribute('height', '4.5')
    model.setAttribute('align', 'center')
    model.setAttribute('scale', '15 15 15')
    model.setAttribute('look-at', '[gps-camera]');
  
  model.addEventListener('loaded', () => {
    window.dispatchEvent(new CustomEvent('gps-entity-place-loaded'))
  });
  
  scene.appendChild(model);

  });
}
